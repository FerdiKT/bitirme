schema = {
    # Schema definition, based on Cerberus grammar. Check the Cerberus project
    # (https://github.com/nicolaiarocci/cerberus) for details.
    'title': {
        'type': 'string',
        'minlength': 5,
        'maxlength': 100,
        'required': True,
    },
    'comment': {
        'type': 'string',
    },
    'userName': {
        'type': 'string'
    },
    'userID': {
        'type': 'string'
    },
    'productID': {
        'type': 'string'
    },
    'date': {
        'type': 'datetime'
    }
}

comments = {
    # 'title' tag used in item links. Defaults to the resource title minus
    # the final, plural 's' (works fine in most cases but not for 'people')
    'item_title': 'product',

    # We choose to override global cache-control directives for this resource.
    'cache_control': 'max-age=10,must-revalidate',
    'cache_expires': 10,

    # most global settings can be overridden at resource level
    'resource_methods': ['GET', 'POST'],

    'schema': schema
}