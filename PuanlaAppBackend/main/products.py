schema = {

    #Fields
    'name': {
        'type': 'string'
    },
    'description': {
        'type': 'string'
    },
    'category': {
        'type': 'list'
    },
    'comments': {
        'type': 'list'
    },
    'productLink': {
        'type': 'string'
    },
    'ownersite': {
        'type': 'string'
    },
    'images': {
        'type': 'string'
    },
    'rating': {
        'type': 'string'
    },
    'brand': {
        'type': 'string'
    },
    'definitionName': {
        'type': 'string'
    }
}

products = {
    # 'title' tag used in item links. Defaults to the resource title minus
    # the final, plural 's' (works fine in most cases but not for 'people')
    'item_title': 'product',

    # We choose to override global cache-control directives for this resource.
    'cache_control': 'max-age=10,must-revalidate',
    'cache_expires': 10,

    # most global settings can be overridden at resource level
    'resource_methods': ['GET', 'POST','DELETE'],

    'schema': schema,
"""
    # Limiting the Fieldset
    'datasource': {
        'projection': {'description': 0, 'images': 0}
    },
"""
    'mongo_indexes': {'text': ([('name', "text")])}
}