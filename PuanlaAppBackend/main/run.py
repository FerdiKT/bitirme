from eve import Eve
import ProductController as p



def post_get_callback(resource, request, payload):
    print('A GET on the "%s" endpoint was just performed!' % resource)

def post_post_callback(resource, request, payload):
    print('A POST on the "%s" endpoint was just performed!' % resource)


app = Eve()
app.on_post_GET += post_get_callback
app.on_post_POST += post_post_callback
# app.on_pre_GET_products += p.pre_products_get_callback
# app.on_pre_POST_products += p.pre_products_post_callback

if __name__ == '__main__':
    app.run("172.31.28.220","5000")
