import requests
import json

while True:
    response = requests.get("http://35.156.45.132:5000/products")
    print(response.text)
    mjson = json.loads(response.text)
    if len(mjson["_items"]) == 0:
        break
    for item in mjson["_items"]:
        headers = {'Content-type': 'application/json', 'If-Match': item["_etag"]}
        # if item["ownersite"] == "Teknosa":
        requests.delete("http://35.156.45.132:5000/products/" + item["_id"], headers=headers)