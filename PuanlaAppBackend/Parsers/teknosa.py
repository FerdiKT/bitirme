import requests
import json
from bs4 import BeautifulSoup
import urllib.request
import Product
import smtplib
import email.message

# links = ["http://www.teknosa.com/"]
links = ["http://www.teknosa.com/urunler/125088756/logitech-prodigy-gaming-headset-981-000627"]

products = []

proxyIndex = 0
for link in links:
    if link[0:1] == "/":
        link = "http://www.teknosa.com"+link
    if link.find("teknosa") == -1:
        continue
    try:
        resp = urllib.request.urlopen(link)
        soup = BeautifulSoup(resp, from_encoding=resp.info().get_param('charset'))

        for href in soup.find_all('a', href=True):
            if href['href'] not in links:
                #print(href['href'])
                links.append(href['href'])

        try:
            page = requests.get(link)

            soup = BeautifulSoup(page.content, 'html.parser')

            comments = soup.find('div', class_='comments')

            productName = soup.find('div', class_='product-name').find('h1')
            productDesc = soup.find('div', id='urun-aciklamasi')
            productImage = "http:" + soup.find('div', class_='product-container').find('div', class_='images').find('img', class_='cloudzoom')["src"]
            print(productImage)
            left = page.text[page.text.find("Teknosa.ProductDetail =") + 24:]
            mJson = left[:left.find('};') + 1]
            print(mJson)
            #print("JSON:" + mJson)
            mJson = json.loads(mJson)
            print(mJson["ProductComputedIndex"])
            cat = requests.get("http://www.teknosa.com/GetProductCatelog.sx?methodName=getInformationsForProduct&productId=" + str(mJson["ProductComputedIndex"]))
            print(cat.content)
            categoryArray = []
            car = json.loads(cat.text)
            for category in car["result"]["breadCrumbList"]:
                categoryArray.append(category["DisplayName"])
                print(category["DisplayName"])
            categoryArray.pop(0)

            if "AKILLI TELEFON" not in categoryArray:
                continue
            items = comments.find_all('div', class_='item')
            commentArray = []
            for item in items:
                print(item.find('div', class_='rating'))
                print(item.find('div', class_='message'))

                commentArray.append({
                                        "ownersite": "Teknosa",
                                        "rating": item.find('div', class_='rating').text,
                                        "reviewText": item.find('div', class_='message').text
                })

            product = {
                        "ownersite": "Teknosa",
                        "name": productName.text,
                        "description": productDesc.text,
                        "images": productImage,
                        "comments": commentArray,
                        "category": categoryArray
                      }
            url = 'http://35.156.45.132:5000/products'

            headers = {'Content-type': 'application/json'}
            r = requests.post(url, data=json.dumps(product), headers=headers)
            print(r.content)
            products.append(Product.Product(productName,commentArray))

        except Exception as inst:
            #print("###")
            print(inst)
            pass

    except Exception as inst:
        print("+++")
        print(inst)
        pass

    if len(products) == 100:
        print(products)
        break


































"""
import requests
from bs4 import BeautifulSoup
import json

page = requests.get('http://www.teknosa.com/urunler/125076139/iphone-7-32gb-black-akilli-telefon')

soup = BeautifulSoup(page.content, 'html.parser')

comments = soup.find('div', class_='comments')

productname = soup.find('div', class_='product-name').find('h1')
productDesc = soup.find('div', id='urun-aciklamasi')

print(productDesc.text)


items = comments.find_all('div', class_='item')
commentArray = []
for item in items:
    print(item.find('div', class_='rating'))
    print(item.find('div', class_='message'))

    commentArray.append({
                            "rating": item.find('div', class_='rating').text,
                            "reviewText": item.find('div', class_='message').text
    })

product = {
            "ownersite": "Teknosa",
            "name": productname.text,
            "description": productDesc.text,
            "comments": commentArray
          }
url = 'http://35.156.45.132:5000/products'

headers = {'Content-type': 'application/json'}
r = requests.post(url, data=json.dumps(product), headers=headers)
print(r.content)
"""