class Product(object):
    name = ""
    review = ""

    # The class "constructor" - It's actually an initializer
    def __init__(self, name, review):
        self.name = name
        self.review = review