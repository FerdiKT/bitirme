//
//  BaseRequest.swift
//  PuanlaApp
//
//  Created by Ferdi Kızıltoprak on 07/03/2017.
//  Copyright © 2017 Ferdi Kızıltoprak. All rights reserved.
//

import UIKit
import Foundation
import Alamofire
import ObjectMapper
import AlamofireObjectMapper


class BaseRequest: Mappable {
    
    var METHOD: HTTPMethod! = .get
    
    var PATH: String! = ""
    
    var PARAMETER: [String: Any] = [:]
    
    var HEADERS: [String: String] = [:]
    
    
    init() {
        
    }
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        
    }
    
}
