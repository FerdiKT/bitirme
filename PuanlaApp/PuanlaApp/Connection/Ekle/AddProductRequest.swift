//
//  AddProductRequest.swift
//  PuanlaApp
//
//  Created by Ferdi Kızıltoprak on 13/04/2017.
//  Copyright © 2017 Ferdi Kızıltoprak. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper
import AlamofireObjectMapper

class AddProductRequest: BaseRequest {

    override init() {
        super.init()
        self.METHOD = .post
        self.PATH   = "products"
    }
    
    required init?(map: Map) {
        super.init(map: map)
        
    }
    
    override func mapping(map: Map) {
        
    }

}
