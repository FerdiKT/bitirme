//
//  ProductUpdateRequest.swift
//  PuanlaApp
//
//  Created by Ferdi Kızıltoprak on 27/04/2017.
//  Copyright © 2017 Ferdi Kızıltoprak. All rights reserved.
//

import UIKit
import ObjectMapper
class ProductUpdateRequest: BaseRequest {
    
    var product: ProductModel?
    
    override init() {
        super.init()
        self.METHOD = .patch
        self.PATH   = "products"
    }
    
    required init?(map: Map) {
        super.init(map: map)
        
    }
    
    override func mapping(map: Map) {
        self.product <- map["product"]
    }
}
