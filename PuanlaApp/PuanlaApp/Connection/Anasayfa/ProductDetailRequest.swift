//
//  ProductDetailRequest.swift
//  PuanlaApp
//
//  Created by Ferdi Kızıltoprak on 28/03/2017.
//  Copyright © 2017 Ferdi Kızıltoprak. All rights reserved.
//

import UIKit
import ObjectMapper

class ProductDetailRequest: BaseRequest {

    var id: String!
    
    override init() {
        super.init()
        self.METHOD = .get
        self.PATH   = "products/"
    }
    
    required init?(map: Map) {
        super.init(map: map)
        
    }
    
    override func mapping(map: Map) {
        self.id <- map["id"]
    }
}
