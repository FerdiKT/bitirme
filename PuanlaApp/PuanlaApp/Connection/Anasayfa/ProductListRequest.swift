//
//  ProductListRequest.swift
//  PuanlaApp
//
//  Created by Ferdi Kızıltoprak on 07/03/2017.
//  Copyright © 2017 Ferdi Kızıltoprak. All rights reserved.
//

import UIKit
import ObjectMapper
import Foundation

class ProductListRequest: BaseRequest {

    override init() {
        super.init()
        self.METHOD = .get
        self.PATH   = "products"
    }
    
    required init?(map: Map) {
        super.init(map: map)
        
    }
    
    override func mapping(map: Map) {
        
    }
    
}
