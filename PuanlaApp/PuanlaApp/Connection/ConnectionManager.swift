//
//  ConnectionManager.swift
//  PuanlaApp
//
//  Created by Ferdi Kızıltoprak on 15/12/2016.
//  Copyright © 2016 Ferdi Kızıltoprak. All rights reserved.
//

import UIKit
//import Firebase
import FirebaseDatabase
import FirebaseAuth
//import FirebaseStorage
import Alamofire
import ObjectMapper
import AlamofireObjectMapper
import PKHUD

class ConnectionManager: NSObject {
    
    static let shared = ConnectionManager()
    
    
    var BASE_URL: String = "http://35.156.45.132:5000/"
    
    func request< T: BaseResponse >(request: BaseRequest, callback: @escaping (_ response : T) -> Void) {
        
        HUD.show(.progress)
        
        let url = BASE_URL + request.PATH
        log.debug(request.PARAMETER)
        Alamofire.request(url, method: request.METHOD, parameters: request.PARAMETER, encoding: request.METHOD != .get ? JSONEncoding.default : URLEncoding.default, headers: request.HEADERS).responseObject { (response: DataResponse<T>) in
            log.debug("#######################RESPONSE#########################")
            //                log.debug(response.request?.url)
            log.debug(response.response?.debugDescription)
            
            if response.result.value != nil {
                callback(response.result.value!)
                log.debug(response.result.value!.toJSONString())
            }
            HUD.hide()
        }
//
//        ConnectionManager.shared.getToken { (token) in
//            request.PARAMETER["token"] = token
//            
//        }
        
    }
    
    
    var usersDBRef = FIRDatabase.database().reference().child("USERS")
    //    var productDBref = FIRDatabase.database().reference().child("PRODUCTS")
    //    var commentDBref = FIRDatabase.database().reference().child("COMMENTS")
    //
    var userID = FIRAuth.auth()?.currentUser?.uid
    
    
    func getToken(callback: @escaping (String?) -> Void) {
        FIRAuth.auth()?.currentUser?.getTokenForcingRefresh(true) {idToken, error in
            if let error = error {
                log.debug(error)
                callback(nil)
            } else {
                callback(idToken)   
            }
        }
    }
    
    //
    //    func sendComment(productID: String, comment: String) -> Bool {
    //
    //        let comment = self.ref.child("products").child("productID").child("comments").childByAutoId()
    //        comment.setValue(["userID": uid!, "comment": link])
    //
    //        return true
    //    }
    //
    func addProduct(vc: UIViewController, product: String){
        //        let pID = usersDBRef.child(userID!).child("PRODUCTS").childByAutoId().key
        //        let pCommentID = usersDBRef.child(userID!).child("PRODUCTS").child(pID).child("COMMENTS").childByAutoId().key
        
        self.usersDBRef.child(userID!).child("COMMENTEDPRODUCTS")
            .childByAutoId()
            .setValue(["product": product]) {
                (error, ref) -> Void in
                if error != nil {
                    log.debug("Ürün ekleme hatası:\(error.debugDescription)")
                    Tools.showPopup(title: "Bilgi", message: "Ürün Ekleme Sırasına Hata oluştu", buttonTitle: "Tamam", viewController: vc, handler: nil)
                }
                else {
                    Tools.showPopup(title: "Bilgi", message: "Ürün Ekleme Başarılı", buttonTitle: "Tamam", viewController: vc, handler: nil)
                }
        }
        
    }
    
    func getProducts(callback: @escaping (_ products : [ProductModel]) -> Void) {
        log.debug("getProducts")
        self.usersDBRef.child(userID!).child("COMMENTEDPRODUCTS").observeSingleEvent(of: .value, with: { (snapshot) in
            
            var products: [ProductModel] = []
            for p in snapshot.children {
                
                let product = (p as! FIRDataSnapshot).value as! [String: String]
                log.debug(product["product"])
                products.append(Mapper<ProductModel>().map(JSONString: product["product"]!)!)
                log.debug(products[0].name)
                
            }
            log.debug(products.count)
            callback(products)
        })
    }
    
//    func getSingleProduct(productID: String, callback: @escaping (_ product : ProductModel) -> Void) {
//        self.usersDBRef.child(userID!).child("PRODUCTS").child(productID).observeSingleEvent(of: .value, with: { (snapshot) in
//            
//            let product = snapshot.value as! String
//            
//            callback(Mapper<ProductModel>().map(JSONString: product)!)
//            
//        })
//    }
    
    func initializeUser() {
        userID = FIRAuth.auth()?.currentUser?.uid
        //self.usersDBRef.child(userID!).setValue("COMMENTEDPRODUCTS")
        //self.usersDBRef.child(userID!).setValue("USERDETAILS")
        //self.usersDBRef.child(userID!).setValue("FAVORITES")
    }
    
    func getUserDetails(callback: @escaping (_ userDetail: [String]) -> Void ) {
        self.usersDBRef.child(userID!).child("USERDETAILS").observeSingleEvent(of: .value, with: { (snapshot) in
            guard snapshot.value != nil else {
                return
            }
            log.debug(snapshot.value)
            if let userDetails = snapshot.value as? [String] {
                callback(userDetails)
            }
        })
    }
    
    func updateUserDetails(vc: UIViewController, userDetails: [String]) {
        self.usersDBRef.child(userID!).child("USERDETAILS").setValue(userDetails) { (error, ref) -> Void in
            if error != nil {
                log.debug("Kullanıcı bilgi güncelleme hatası:\(error.debugDescription)")
                Tools.showPopup(title: "Bilgi", message: "Bilgilerinizi güncelleme sırasında bir hata oluştu.", buttonTitle: "Tamam", viewController: vc, handler: nil)
            }
            else {
                Tools.showPopup(title: "Bilgi", message: "Bilgileriniz güncellenmiştir.", buttonTitle: "Tamam", viewController: vc, handler: nil)
            }
        }
    }
    
    func getFavorites(callback: @escaping (_ products: [String: ProductModel]?) -> Void) {
        self.usersDBRef.child(userID!).child("FAVORITES").observeSingleEvent(of: .value, with: { (snapshot) in
            log.debug("Favorites:\(String(describing: snapshot.value))")
            
            var products: [String: ProductModel] = [:]
            for p in snapshot.children {
                
                let product = (p as! FIRDataSnapshot).value as! [String: String]
                log.debug(product["product"])
                let mp = Mapper<ProductModel>().map(JSONString: product["product"]!)!
                products[mp.id!] = mp
            }
            log.debug(products.count)
            callback(products)
        })
    }
    
    func addFavorites(product: ProductModel, callback: @escaping (_ error: Error?) -> Void) {
        self.usersDBRef.child(userID!).child("FAVORITES").child(product.id!).setValue(["product": product.toJSONString()]) { (error, ref) -> Void in
            log.debug(error)
            callback(error)
        }
    }
    func removeFavorite(product: ProductModel, callback: @escaping (_ error: Error?) -> Void) {
        self.usersDBRef.child(userID!).child("FAVORITES").child(product.id!).removeValue() { (error, ref) -> Void in
            log.debug(error)
            callback(error)
        }
    }
    
    
    //    func loadImage() {
    //        let storageUrl = FIRApp.defaultApp()?.options.storageBucket
    //        var storageRef = FIRStorage.storage().reference(forURL: "gs://" + storageUrl!)
    //        FIRStorage.storage().reference(forURL: imageURL).data(withMaxSize: INT64_MAX) {(data, error) in
    //            if let error = error {
    //                print("Error downloading: \(error)")
    //                return
    //            }
    //            cell.imageView?.image = UIImage.init(data: data!)
    //            tableView.reloadData()
    //        }
    //    }
}





