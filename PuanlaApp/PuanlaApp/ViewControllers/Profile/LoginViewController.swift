//
//  LoginViewController.swift
//  PuanlaApp
//
//  Created by Ferdi Kızıltoprak on 08/12/2016.
//  Copyright © 2016 Ferdi Kızıltoprak. All rights reserved.
//

import UIKit
import Firebase

class LoginViewController: BaseViewController {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var nameTextfield: UITextField!
    @IBOutlet weak var emailTextfield: UITextField!
    @IBOutlet weak var passwordTextfield: UITextField!
    
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var signupButton: UIButton!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        passwordTextfield.isSecureTextEntry = true
        /*
        if (FIRAuth.auth()?.currentUser) != nil
        {
            let HomeViewController = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
            navigationController?.pushViewController(HomeViewController, animated: true)
        }
        */
        loginButton.addTarget(self, action: #selector(login), for: .touchUpInside)
        signupButton.addTarget(self, action: #selector(createAccount), for: .touchUpInside)
    
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        tap?.cancelsTouchesInView = false
    }
    

    func login() {
        
        self.nameTextfield.isHidden = false
        self.nameLabel.isHidden = false
        
        if self.emailTextfield.text == "" || self.passwordTextfield.text == ""
        {
            let alertController = UIAlertController(title: "Uyarı", message: "Lütfen Email ve şifre giriniz.", preferredStyle: .alert)
            
            let defaultAction = UIAlertAction(title: "Tamam", style: .cancel, handler: nil)
            alertController.addAction(defaultAction)
            
            self.present(alertController, animated: true, completion: nil)
        }
        else
        {
            FIRAuth.auth()?.signIn(withEmail: self.emailTextfield.text!, password: self.passwordTextfield.text!) { (user, error) in
                
                if error == nil {
                    ConnectionManager.shared.initializeUser()
                    
                    let userInfo = ["email": self.emailTextfield.text!, "password": self.passwordTextfield.text!]
                    UserDefaults.standard.set(userInfo, forKey: "userInformation")
                    
                    UserDefaults.standard.setValue(true, forKey: "isUserLogin")
                    let storyboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let tabBarController:UIViewController = storyboard.instantiateViewController(withIdentifier: "UITabBarController") as! UITabBarController
                    UIApplication.shared.keyWindow?.rootViewController = tabBarController
                    log.debug("Login successfull")
                    
                }
                else {
                    let alertController = UIAlertController(title: "Uyarı", message: error?.localizedDescription, preferredStyle: .alert)
                    
                    let defaultAction = UIAlertAction(title: "Tamam", style: .cancel, handler: nil)
                    alertController.addAction(defaultAction)
                    
                    self.present(alertController, animated: true, completion: nil)
                }
                
            }
        }
    }
    func createAccount() {
        
        self.nameTextfield.isHidden = false
        self.nameLabel.isHidden = false
        
        if self.nameTextfield.text == "" || self.emailTextfield.text == "" || self.passwordTextfield.text == ""
        {
            let alertController = UIAlertController(title: "Uyarı", message: "Lütfen İsim, Email ve şifre giriniz.", preferredStyle: .alert)
            
            let defaultAction = UIAlertAction(title: "Tamam", style: .cancel, handler: nil)
            alertController.addAction(defaultAction)
            
            self.present(alertController, animated: true, completion: nil)
        }
        else
        {
            FIRAuth.auth()?.createUser(withEmail: self.emailTextfield.text!, password: self.passwordTextfield.text!) { (user, error) in
                
                if error == nil {
                    
                    let storageRef = FIRStorage.storage().reference().child("usersProfilePics").child(user!.uid)
                    let imageData = UIImageJPEGRepresentation(UIImage(named:"default profile")!, 0.1)
                    storageRef.put(imageData!, metadata: nil, completion: { (metadata, err) in
                        if err == nil {
                            let path = metadata?.downloadURL()?.absoluteString
                            let values = ["name": self.nameTextfield.text!, "email": self.emailTextfield.text!, "profilePicLink": path!]
                            FIRDatabase.database().reference().child("USERS").child((user?.uid)!).child("credentials").updateChildValues(values, withCompletionBlock: { (errr, _) in
                                if errr == nil {
                                    let userInfo = ["email" : self.emailTextfield.text, "password" : self.passwordTextfield.text]
                                    UserDefaults.standard.set(userInfo, forKey: "userInformation")
                                }
                            })
                        }
                    })
                    
                    
                    ConnectionManager.shared.initializeUser()
                    UserDefaults.standard.setValue(true, forKey: "isUserLogin")
                    let storyboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let tabBarController:UIViewController = storyboard.instantiateViewController(withIdentifier: "UITabBarController") as! UITabBarController
                    UIApplication.shared.keyWindow?.rootViewController = tabBarController
                    log.debug("Login successfull")
                    
                }
                else {
                    let alertController = UIAlertController(title: "Uyarı", message: error?.localizedDescription, preferredStyle: .alert)
                    
                    let defaultAction = UIAlertAction(title: "Tamam", style: .cancel, handler: nil)
                    alertController.addAction(defaultAction)
                    
                    self.present(alertController, animated: true, completion: nil)
                }
                
            }
        }
    }
}
