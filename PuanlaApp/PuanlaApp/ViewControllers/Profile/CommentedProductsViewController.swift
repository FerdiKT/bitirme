//
//  CommentedProductsViewController.swift
//  PuanlaApp
//
//  Created by Ferdi Kızıltoprak on 12/05/2017.
//  Copyright © 2017 Ferdi Kızıltoprak. All rights reserved.
//

import UIKit

class CommentedProductsViewController: UITableViewController {

    var products:[ProductModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Yorumladıklarım"
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        getProducts()
        
    }
    
    func getProducts() {
        ConnectionManager.shared.getProducts { (products) in
            if products.count > 0 {
                self.products = products
                self.tableView.reloadData()
            }
            else {
                log.debug("Product Yok")
            }
//            if self.refreshControl!.isRefreshing {
//                self.refreshControl!.endRefreshing()
//            }
        }
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return products.count
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 176
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "HomeTableViewCell") as! HomeTableViewCell
        
        if products.count == 0 {
            cell.productNameLabel.text = "Ürün Bulunamadı."
            cell.commentCountLabel.text = ""
            return cell
        }
        
        if self.products[indexPath.row].images != nil {
            log.debug(self.products[indexPath.row].images!)
            cell.productImageView.kf.setImage(with: URL(string: products[indexPath.row].images!))
        }
        cell.productNameLabel.text = products[indexPath.row].name!
        cell.commentCountLabel.text = "Yorum Adedi: \(products[indexPath.row].comments!.count)"
        cell.ownerSiteImage.image = UIImage(named: products[indexPath.row].ownersite!)
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("index:\(indexPath.row)")
        let productDetailViewController = self.storyboard?.instantiateViewController(withIdentifier: "ProductDetailViewController") as! ProductDetailViewController
        productDetailViewController.productID = self.products[indexPath.row].id!
        self.navigationController?.pushViewController(productDetailViewController, animated: true)
    }


}
