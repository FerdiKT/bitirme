//
//  AccountInfoViewController.swift
//  PuanlaApp
//
//  Created by Ferdi Kızıltoprak on 13/05/2017.
//  Copyright © 2017 Ferdi Kızıltoprak. All rights reserved.
//

import UIKit

class AccountInfoViewController: UITableViewController {

    var userDetailTitles: [String] = ["İsim","Soyisim","Email"]
    var userDetails: [String] = ["","",""]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Üyelik Bilgilerim"

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateUserDetails()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func updateUserDetails() {
        ConnectionManager.shared.getUserDetails { (userDetails) in
            self.userDetails = userDetails
            self.tableView.reloadData()
        }
    }
    

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return userDetails.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)

        cell.textLabel?.text = userDetailTitles[indexPath.row]
        cell.detailTextLabel?.text = userDetails[indexPath.row]

        return cell
    }
 
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        changeUserInfoPopup(index: indexPath.row)
    }
    
    func changeUserInfoPopup(index: Int) {
        //1. Create the alert controller.
        let alert = UIAlertController(title: userDetailTitles[index], message: nil, preferredStyle: .alert)
        
        //2. Add the text field. You can configure it however you need.
        alert.addTextField { (textField) in
            textField.text = self.userDetails[index]
        }
        
        // 3. Grab the value from the text field, and print it when the user clicks OK.
        alert.addAction(UIAlertAction(title: "Kaydet", style: .default, handler: { [weak alert] (_) in
            let textField = alert?.textFields![0] // Force unwrapping because we know it exists.
            log.debug("Text field: \(String(describing: textField!.text))")
            
            self.userDetails[index] = textField!.text!
            ConnectionManager.shared.updateUserDetails(vc: self, userDetails: self.userDetails)
            self.updateUserDetails()
        }))
        alert.addAction(UIAlertAction(title: "İptal", style: .cancel, handler: nil))
        // 4. Present the alert.
        self.present(alert, animated: true, completion: nil)
    }
}






