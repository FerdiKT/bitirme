//
//  ProfileViewController.swift
//  PuanlaApp
//
//  Created by Ferdi Kızıltoprak on 19/11/2016.
//  Copyright © 2016 Ferdi Kızıltoprak. All rights reserved.
//

import UIKit
import FirebaseAuth

class ProfileViewController: BaseViewController {

    var menuTitles:[String] = ["Üyelik Bilgilerim","Yorumladığım Ürünler", "Favorilerim"]
    
    @IBOutlet weak var tableView: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    @IBAction func logoutAction(_ sender: Any) {
        do {
            try FIRAuth.auth()?.signOut()
            UserDefaults.standard.setValue(false, forKey: "isUserLogin")
            UserDefaults.standard.removeObject(forKey: "userInformation")
            log.debug("Çıkış Başarılı")
            
            let storyboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let loginViewController:UIViewController = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
            UIApplication.shared.keyWindow?.rootViewController = loginViewController
        } catch {
            Tools.showPopup(title: "Uyarı", message: "Çıkış sırasında bir hata oluştu. Lütfen tekrar deneyiniz.", buttonTitle: "Tamam", viewController: self, handler: nil)
        }
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
    }
}

extension ProfileViewController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuTitles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")!
        
        cell.textLabel?.text = menuTitles[indexPath.row]
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        log.debug("index:\(indexPath.row)")
        switch indexPath.row {
        case 0:
            let productDetailViewController = self.storyboard?.instantiateViewController(withIdentifier: "AccountInfoViewController") as! AccountInfoViewController
            self.navigationController?.pushViewController(productDetailViewController, animated: true)
        case 1:
            let productDetailViewController = self.storyboard?.instantiateViewController(withIdentifier: "CommentedProductsViewController") as! CommentedProductsViewController
            self.navigationController?.pushViewController(productDetailViewController, animated: true)
        case 2:
            let productDetailViewController = self.storyboard?.instantiateViewController(withIdentifier: "FavoritesViewController") as! FavoritesViewController
            self.navigationController?.pushViewController(productDetailViewController, animated: true)
            
        default:
            break
        }
        
    }
}
