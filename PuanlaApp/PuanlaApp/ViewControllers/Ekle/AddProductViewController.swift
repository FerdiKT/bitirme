//
//  AddProductViewController.swift
//  PuanlaApp
//
//  Created by Ferdi Kızıltoprak on 27/12/2016.
//  Copyright © 2016 Ferdi Kızıltoprak. All rights reserved.
//

import UIKit

class AddProductViewController: BaseViewController {
    @IBOutlet weak var pNameTextfield: UITextField!
    @IBOutlet weak var pDetailTextview: UITextView!
    @IBOutlet weak var addProductButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        addProductButton.addTarget(self, action: #selector(addProduct), for: .touchUpInside)
    }
    
    func addProduct() {
        let product = ["ownersite": "Teknosa",
                       "name" : pNameTextfield.text!,
                       "description": pDetailTextview.text,
//                       "comments": commentArray
                      ] as [String : Any]
        
        
        let addProductRequest = AddProductRequest()
        
        addProductRequest.PARAMETER = product
        
        ConnectionManager.shared.request(request: addProductRequest) { (response: BaseResponse) in
            
        }
    }
}
