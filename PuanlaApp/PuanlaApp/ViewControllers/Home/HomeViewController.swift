//
//  HomeViewController.swift
//  PuanlaApp
//
//  Created by Ferdi Kızıltoprak on 19/11/2016.
//  Copyright © 2016 Ferdi Kızıltoprak. All rights reserved.
//

import UIKit
//import FirebaseDatabase

class HomeViewController: BaseViewController {
    
    var products: [ProductModel] = []
    
    let request = ProductListRequest()
    
    var page: Int = 1
    var isReachTotalResult = false
    
    @IBOutlet weak var tableview: UITableView!
    
    let filterButton = UIButton(type: .custom)
    var item: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        filterButton.setImage(UIImage(named: "filter"), for: .normal)
        filterButton.frame = CGRect(x: 5, y: 5, width: 25, height: 25)
        filterButton.addTarget(self, action: #selector(openFilterView), for: .touchUpInside)
        
        item = UIBarButtonItem(customView: filterButton)
        
        self.navigationItem.rightBarButtonItem = item

        productListServiceCall()
    }
    
    // MARK: - Open Filter View
    
    func openFilterView() {
        let filterViewController = self.storyboard?.instantiateViewController(withIdentifier: "FilterViewController") as! FilterViewController
        filterViewController.delegate = self
        self.navigationController?.pushViewController(filterViewController, animated: true)
    }
    
    //Service Calls
    func productListServiceCall() {
        request.PARAMETER["page"] = "\(page)"
        ConnectionManager.shared.request(request: request) { (response: ProductListResponse) in
            log.info(response.items)
            self.products.append(contentsOf: response.items!)
            if response.meta!.total! <= self.products.count {
                self.isReachTotalResult = true
            }
            self.tableview.reloadData()
        }
    }
}

extension HomeViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return products.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 176
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "HomeTableViewCell") as! HomeTableViewCell
        cell.productImageView.image = nil
        if self.products[indexPath.row].images != nil {
            log.debug(self.products[indexPath.row].images!)
            cell.productImageView.kf.setImage(with: URL(string: products[indexPath.row].images!))
        }
        cell.productNameLabel.text = products[indexPath.row].name!
        cell.commentCountLabel.text = "Yorum Adedi: \(products[indexPath.row].comments!.count)"
        cell.ownerSiteImage.image = UIImage(named: products[indexPath.row].ownersite!)
        
        if indexPath.row == products.count - 1 && !isReachTotalResult {
            page += 1
            productListServiceCall()
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("index:\(indexPath.row)")
        let productDetailViewController = self.storyboard?.instantiateViewController(withIdentifier: "ProductDetailViewController") as! ProductDetailViewController
        productDetailViewController.productID = self.products[indexPath.row].id!
        self.navigationController?.pushViewController(productDetailViewController, animated: true)
    }
}

extension HomeViewController: FilterViewControllerDelegate {
    func didEndSelectingFilter(_ filters: [String : String]) {
        request.PARAMETER = filters
        products.removeAll()
        productListServiceCall()
    }
}





