//
//  ProductDetailImageTableViewCell.swift
//  PuanlaApp
//
//  Created by Ferdi Kızıltoprak on 17/03/2017.
//  Copyright © 2017 Ferdi Kızıltoprak. All rights reserved.
//

import UIKit

class ProductDetailImageTableViewCell: UITableViewCell {

    @IBOutlet weak var mImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
