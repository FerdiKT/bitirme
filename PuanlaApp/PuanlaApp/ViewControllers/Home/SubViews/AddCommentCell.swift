//
//  AddCommentCell.swift
//  PuanlaApp
//
//  Created by Ferdi Kızıltoprak on 25/04/2017.
//  Copyright © 2017 Ferdi Kızıltoprak. All rights reserved.
//

import UIKit

class AddCommentCell: UITableViewCell {

    @IBOutlet weak var addButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
