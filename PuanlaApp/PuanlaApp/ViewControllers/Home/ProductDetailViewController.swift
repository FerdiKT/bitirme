
//  ProductDetailViewController.swift
//  PuanlaApp
//
//  Created by Ferdi Kızıltoprak on 19/11/2016.
//  Copyright © 2016 Ferdi Kızıltoprak. All rights reserved.
//

import UIKit
import Kingfisher
import AlamofireObjectMapper
import Alamofire

class ProductDetailViewController: BaseViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var productID: String!
    var product: ProductModel?
    var selectedIndex: Int = 0
    var favorites: [String:ProductModel] = [:]
    
    var productUpdateRequest = ProductUpdateRequest()
    
    let favoriButton = UIButton(type: .custom)
    var item: UIBarButtonItem!
    
    @IBOutlet weak var productNameLabel: UILabel!
    @IBOutlet weak var tableview: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getProductDetailServiceCall()
        getFavorites()
        
        
        tableView.estimatedRowHeight = 100.0
        tableView.rowHeight = UITableViewAutomaticDimension
        
        favoriButton.setImage(UIImage(named: "star"), for: .normal)
        favoriButton.frame = CGRect(x: 5, y: 5, width: 25, height: 25)
        favoriButton.addTarget(self, action: #selector(addFavorites), for: .touchUpInside)
        
        item = UIBarButtonItem(customView: favoriButton)
        
        self.navigationItem.rightBarButtonItem = item
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        tableView.reloadData()
    }
    
    func getProductDetailServiceCall() {
        let request = ProductDetailRequest()
        
        let jsonObject = NSMutableDictionary()
        jsonObject.setValue(productID, forKey: "_id")
        if let theJSONData = try? JSONSerialization.data(
            withJSONObject: jsonObject,
            options: []) {
            let theJSONText = String(data: theJSONData,
                                     encoding: .ascii)
            print("JSON string = \(theJSONText!)")
            
            request.PARAMETER["where"] = theJSONText!
            ConnectionManager.shared.request(request: request) { (response: ProductListResponse) in
                log.info(response.items)
                self.product = response.items![0]
                log.info(self.product?.name)
                self.tableview.reloadData()
            }
        }
    }
    
    func getFavorites() {
        ConnectionManager.shared.getFavorites { (favorites) in
            if favorites != nil {
                self.favorites = favorites!
                if self.favorites[self.productID] != nil {
                    self.favoriButton.setImage(UIImage(named: "starred"), for: .normal)
                }
            }
        }
    }
    
    func addFavorites() {
        log.debug("ADD FAVORİTES")
        
        guard product != nil else {
            return
        }
        
        log.debug(product!.name!)
        
        if favorites[productID]?.id != product?.id {
            ConnectionManager.shared.addFavorites(product: product!) { (error) in
                if error != nil {
                    log.debug("Favori ekleme hatası:\(error.debugDescription)")
                    Tools.showPopup(title: "Bilgi", message: "Favorilere ekleme sırasında bir hata oluştu.", buttonTitle: "Tamam", viewController: self, handler: nil)
                }
                else {
                    Tools.showPopup(title: "Bilgi", message: "Ürün favorilerinize eklenmiştir.", buttonTitle: "Tamam", viewController: self, handler: nil)
                    self.favoriButton.setImage(UIImage(named: "starred"), for: .normal)
                    self.getFavorites()
                }
            }
        }
        else {
            favorites.removeValue(forKey: productID)
            self.favoriButton.setImage(UIImage(named: "star"), for: .normal)
            ConnectionManager.shared.removeFavorite(product: product!, callback: { (error) in
                if error != nil {
                    log.debug("Favori çıkarma hatası:\(error.debugDescription)")
                    Tools.showPopup(title: "Bilgi", message: "Favorilerden çıkarma sırasında bir hata oluştu.", buttonTitle: "Tamam", viewController: self, handler: nil)
                }
                else {
                    Tools.showPopup(title: "Bilgi", message: "Ürün favorilerinizden çıkarılmıştır.", buttonTitle: "Tamam", viewController: self, handler: nil)
                    self.favoriButton.setImage(UIImage(named: "star"), for: .normal)
                    self.getFavorites()
                }

            })
        }
    }
}

extension ProductDetailViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if product != nil {
            return 3 + product!.comments!.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 220
        }
        else if indexPath.row == 1 {
            return 100
        }
         /*
        else if indexPath.row == selectedIndex && (product?.comments?[indexPath.row - 2].reviewText?.characters.count)! > 80 {
            return 150
        }*/
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProductDetailImageTableViewCell") as! ProductDetailImageTableViewCell
            cell.mImageView.image = nil
            if product!.images != nil {
                log.debug(URL(string: self.product!.images!))
                cell.mImageView.kf.setImage(with: URL(string: product!.images!))
            }
            
            return cell
        }
        else if indexPath.row == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProductDetailTableViewCell") as! ProductDetailTableViewCell
            cell.productNameLabel.text = product?.name
            cell.productDescriptionLabel.text = product?.descriptionValue
            cell.commentCountLabel.text = "Yorum Adedi: \(product!.comments!.count)"
            return cell
        }
        else if indexPath.row == product!.comments!.count + 2 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "AddCommentCell") as! AddCommentCell
            cell.addButton.addTarget(self, action: #selector(addCommentPopup), for: .touchUpInside)
            
            return cell
        }
        else if indexPath.row > 1 && product?.comments != nil {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CommentTableViewCell") as! CommentTableViewCell
            cell.commentLabel.text = product?.comments?[indexPath.row - 2].reviewText
//            cell.commentLabel.sizeToFit()
            log.debug(self.product!.ownersite!)
            cell.ownerSiteImage.image = UIImage(named: (product?.comments?[indexPath.row - 2].ownersite)!)
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedIndex = indexPath.row
        if indexPath.row > 1 {
            contactUser(indexPath.row - 2)
        }
        self.tableview.reloadData()
    }
    
    func addCommentPopup() {
        //1. Create the alert controller.
        let alert = UIAlertController(title: "Yorum", message: nil, preferredStyle: .alert)
        
        //2. Add the text field. You can configure it however you need.
        alert.addTextField { (textField) in
            textField.placeholder = "yorumunuz..."
        }
        
        // 3. Grab the value from the text field, and print it when the user clicks OK.
        alert.addAction(UIAlertAction(title: "Ekle", style: .default, handler: { [weak alert] (_) in
            let textField = alert?.textFields![0] // Force unwrapping because we know it exists.
            log.debug("Text field: \(String(describing: textField!.text))")
            let comment = Comments()
            comment.reviewText = textField!.text
            comment.ownersite = "PuanLaApp"
            comment.userID = ConnectionManager.shared.userID
            self.product?.comments?.append(comment)
//            self.product?.toJSON()
            self.productUpdateRequest.PARAMETER = ["comments" : self.product!.comments!.toJSON()]
            self.productUpdateRequest.HEADERS = ["Content-type":"application/json" ,"If-Match":self.product!.etag!]
            self.productUpdateRequest.PATH = "products/" + self.product!.id!
            ConnectionManager.shared.request(request: self.productUpdateRequest, callback: { (response:ProductListResponse?) in
                if response != nil {
                    self.getProductDetailServiceCall()
                    ConnectionManager.shared.addProduct(vc: self, product: self.product!.toJSONString()!)
                }
            })
        }))
        alert.addAction(UIAlertAction(title: "İptal", style: .cancel, handler: nil))
        // 4. Present the alert.
        self.present(alert, animated: true, completion: nil)
    }
    
    func contactUser(_ index: Int) {
        
        
            log.debug("OwnerSite" + (self.product?.comments?[index].ownersite)!)
            if self.product?.comments?[index].ownersite == "PuanLaApp" {
                if let userID = self.product?.comments?[index].userID {
                    if userID != ConnectionManager.shared.userID {
                        //1. Create the alert controller.
                        let alert = UIAlertController(title: "Mesaj Gönder", message: "Yorum sahibine mesaj göndermek istiyor musunuz?", preferredStyle: .alert)
                        
                        alert.addAction(UIAlertAction(title: "Evet", style: .default, handler: { _ in
                            self.tabBarController?.selectedIndex = 2
                            User.info(forUserID: userID, completion: { (user) in
                                let userInfo = ["user": user]
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "showUserMessages"), object: nil, userInfo: userInfo)
                            })
                            
                        }))
                        alert.addAction(UIAlertAction(title: "Hayır", style: .cancel, handler: nil))
                        // 4. Present the alert.
                        self.present(alert, animated: true, completion: nil)
                    }
                }
            }
        
 
    }
}





