//
//  FilterViewController.swift
//  PuanlaApp
//
//  Created by Ferdi Kızıltoprak on 12/05/2017.
//  Copyright © 2017 Ferdi Kızıltoprak. All rights reserved.
//

import UIKit

protocol FilterViewControllerDelegate {
    func didEndSelectingFilter(_ filters: [String:String])
}

class FilterViewController: UITableViewController {

    var delegate: FilterViewControllerDelegate?
    
    enum sections:Int {
        case seller = 0, category, COUNT
    }
    
    var categories: [String] = ["Xiaomi Akıllı Bileklikler","iPhone iOS Telefonlar"]
    var sellers: [String] = ["Teknosa", "Hepsiburada"]
    
    var selectedSellers:[String] = []
    var selectedCats:[String] = []
    
    let doneButton = UIButton(type: .custom)
    var item: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
//        doneButton.setTitle("Uygula", for: .normal)
        doneButton.setImage(UIImage(named: "filter"), for: .normal)
        doneButton.frame = CGRect(x: 5, y: 5, width: 25, height: 25)
        doneButton.addTarget(self, action: #selector(returnFilters), for: .touchUpInside)
        
        item = UIBarButtonItem(customView: doneButton)
        
        self.navigationItem.rightBarButtonItem = item
    }
    
    func returnFilters() {
        var filters: [String:String] = [:]
        var filter: [String] = []
        
        var sellers: String = ""
        if selectedSellers.count > 0 {
            sellers = "\"ownersite\": {\"$in\": ["
            for item in selectedSellers {
                sellers += "\"\(item)\","
            }
            sellers.remove(at: sellers.index(before: sellers.endIndex))
            sellers.append("]}")
        }
        if sellers != ""  {
            filter.append(sellers)
        }
        var cats: String = ""
        if selectedCats.count > 0 {
            cats = "\"category\": {\"$in\": ["
            for item in selectedCats {
                cats += "\"\(item)\","
            }
            cats.remove(at: cats.index(before: cats.endIndex))
            cats.append("]}")
        }
        if cats != ""  {
            filter.append(cats)
        }
        filters["where"] = "{\(filter.joined(separator: ","))}"
        delegate?.didEndSelectingFilter(filters)
        self.navigationController?.popViewController(animated: true)
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return sections.COUNT.rawValue
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch section {
        case sections.seller.rawValue:
            return sellers.count
        case sections.category.rawValue:
            return categories.count
        default:
            break
        }
        
        return 0
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case sections.seller.rawValue:
            return "Satıcı"
        case sections.category.rawValue:
            return "Kategori"
        default:
            return ""
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.detailTextLabel?.text = ""
        
        switch indexPath.section {
        case sections.seller.rawValue:
            cell.textLabel?.text = sellers[indexPath.row]
            if selectedSellers.contains(sellers[indexPath.row]) {
                cell.detailTextLabel?.text = "✓"
            }
        case sections.category.rawValue:
            cell.textLabel?.text = categories[indexPath.row]
            if selectedCats.contains(categories[indexPath.row]) {
                cell.detailTextLabel?.text = "✓"
            }
        default:
            break
        }

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch indexPath.section {
        case sections.seller.rawValue:
            if selectedSellers.contains(sellers[indexPath.row]) {
                selectedSellers.remove(at:selectedSellers.index(of: sellers[indexPath.row])!)
            }
            else {
                selectedSellers.append(sellers[indexPath.row])
            }
            log.debug(selectedSellers)
            break
        case sections.category.rawValue:
            if selectedCats.contains(categories[indexPath.row]) {
                selectedCats.remove(at:selectedCats.index(of: categories[indexPath.row])!)
            }
            else {
                selectedCats.append(categories[indexPath.row])
            }
            log.debug(selectedCats)
            break
        default:
            break
        }
        tableView.reloadData()
    }
}





