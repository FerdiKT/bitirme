//
//  SearchViewController.swift
//  PuanlaApp
//
//  Created by Ferdi Kızıltoprak on 19/11/2016.
//  Copyright © 2016 Ferdi Kızıltoprak. All rights reserved.
//

import UIKit

class SearchViewController: BaseViewController {


    @IBOutlet weak var textField: UITextField!
    
    var searchText: String = ""
    
    @IBAction func search(_ sender: Any) {
//        self.view.endEditing(true)
//        textField.resignFirstResponder()
        navigationController?.view.endEditing(true)
        searchText = textField.text!.lowercased()
        productSearchServiceCall(text: searchText)
        products = []
    }
    var products: [ProductModel] = []
    
    var page: Int = 1
    var isReachTotalResult = false
    
    @IBOutlet weak var tableview: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
    //Service Calls
    func productSearchServiceCall(text: String) {
        
        let json = "{\"$text\" : {\"$search\": \"\(text)\"}}"

        log.debug("JSON:" + json)
        
        let request = ProductListRequest()
        request.PARAMETER["page"] = "\(page)"
        request.PARAMETER["where"] = json
        ConnectionManager.shared.request(request: request) { (response: ProductListResponse) in
            if response.items != nil {
                log.info(response.items)
                self.products.append(contentsOf: response.items!)
                if response.meta!.total! <= self.products.count {
                    self.isReachTotalResult = true
                }
                self.tableview.reloadData()
            }
            else {
                self.products = []
                self.tableview.reloadData()
            }
        }
    }
}

extension SearchViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if products.count == 0 {
            return 1
        }
        return products.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 176
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "HomeTableViewCell") as! HomeTableViewCell
        
        if products.count == 0 {
            cell.productImageView.image = nil
            cell.ownerSiteImage.image = nil
            cell.productNameLabel.text = "Ürün Bulunamadı."
            cell.commentCountLabel.text = ""
            return cell
        }
        
        if self.products[indexPath.row].images != nil {
            log.debug(self.products[indexPath.row].images!)
            cell.productImageView.kf.setImage(with: URL(string: products[indexPath.row].images!))
        }
        cell.productNameLabel.text = products[indexPath.row].name!
        cell.commentCountLabel.text = "Yorum Adedi: \(products[indexPath.row].comments!.count)"
        cell.ownerSiteImage.image = UIImage(named: products[indexPath.row].ownersite!)
        if indexPath.row == products.count - 1  && !isReachTotalResult {
            page += 1
            productSearchServiceCall(text: searchText)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("index:\(indexPath.row)")
        guard self.products.count != 0 else {
            return
        }
        let productDetailViewController = self.storyboard?.instantiateViewController(withIdentifier: "ProductDetailViewController") as! ProductDetailViewController
        productDetailViewController.productID = self.products[indexPath.row].id!
        self.navigationController?.pushViewController(productDetailViewController, animated: true)
    }
}

extension SearchViewController: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        searchText = textField.text!.lowercased()
        productSearchServiceCall(text: searchText)
        products = []
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
//        self.view.endEditing(true)
        navigationController?.view.endEditing(true)
        return true
    }
}
