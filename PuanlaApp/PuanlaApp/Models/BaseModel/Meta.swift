//
//  Meta.swift
//
//  Created by Ferdi Kızıltoprak on 06/03/2017
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public class Meta: Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let total = "total"
    static let maxResults = "max_results"
    static let page = "page"
  }

  // MARK: Properties
  public var total: Int?
  public var maxResults: Int?
  public var page: Int?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    total <- map[SerializationKeys.total]
    maxResults <- map[SerializationKeys.maxResults]
    page <- map[SerializationKeys.page]
  }
}
