//
//  Self.swift
//
//  Created by Ferdi Kızıltoprak on 06/03/2017
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public class Mself: Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let title = "title"
    static let href = "href"
  }

  // MARK: Properties
  public var title: String?
  public var href: String?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    title <- map[SerializationKeys.title]
    href <- map[SerializationKeys.href]
  }
}
