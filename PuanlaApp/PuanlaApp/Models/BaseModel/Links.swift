//
//  Links.swift
//
//  Created by Ferdi Kızıltoprak on 06/03/2017
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public class Links: Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let mSelf = "self"
    static let parent = "parent"
  }

  // MARK: Properties
  public var mSelf: Mself?
  public var parent: Parent?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    mSelf <- map[SerializationKeys.mSelf]
    parent <- map[SerializationKeys.parent]
  }
}
