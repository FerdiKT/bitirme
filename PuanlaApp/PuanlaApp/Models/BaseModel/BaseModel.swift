//
//  BaseModel.swift
//  PuanlaApp
//
//  Created by Ferdi Kızıltoprak on 01/03/2017.
//  Copyright © 2017 Ferdi Kızıltoprak. All rights reserved.
//

import UIKit
import ObjectMapper

class BaseModel: Mappable {

    init() {
    }
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
    }
}
