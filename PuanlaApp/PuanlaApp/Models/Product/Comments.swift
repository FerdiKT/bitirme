//
//  Comments.swift
//
//  Created by Ferdi Kızıltoprak on 13/03/2017
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

class Comments: BaseModel {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let id = "id"
    static let userLocation = "userLocation"
    static let disagreeCount = "disagreeCount"
    static let userAge = "userAge"
    static let reviewText = "reviewText"
    static let date = "date"
    static let title = "title"
    static let rating = "rating"
    static let isPrivate = "isPrivate"
    static let userName = "userName"
    static let gender = "gender"
    static let agreeCount = "agreeCount"
    static let ownersite = "ownersite"
    static let userID = "userID"
  }

  // MARK: Properties
  public var id: String?
  public var userLocation: String?
  public var disagreeCount: Int?
  public var userAge: Int?
  public var reviewText: String?
  public var date: String?
  public var title: String?
  public var rating: String?
  public var isPrivate: Bool? = false
  public var userName: String?
  public var gender: Int?
  public var agreeCount: Int?
  public var ownersite: String?
  public var userID: String?
    
  override init() {
        super.init()
  }

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){
     super.init(map: map)
  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public override func mapping(map: Map) {
    id <- map[SerializationKeys.id]
    userLocation <- map[SerializationKeys.userLocation]
    disagreeCount <- map[SerializationKeys.disagreeCount]
    userAge <- map[SerializationKeys.userAge]
    reviewText <- map[SerializationKeys.reviewText]
    date <- map[SerializationKeys.date]
    title <- map[SerializationKeys.title]
    rating <- map[SerializationKeys.rating]
    isPrivate <- map[SerializationKeys.isPrivate]
    userName <- map[SerializationKeys.userName]
    gender <- map[SerializationKeys.gender]
    agreeCount <- map[SerializationKeys.agreeCount]
    ownersite <- map[SerializationKeys.ownersite]
    userID <- map[SerializationKeys.userID]
  }
}
