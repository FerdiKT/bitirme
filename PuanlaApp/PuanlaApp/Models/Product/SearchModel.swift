//
//  SearchModel.swift
//  PuanlaApp
//
//  Created by Ferdi Kızıltoprak on 12/04/2017.
//  Copyright © 2017 Ferdi Kızıltoprak. All rights reserved.
//

import UIKit
import ObjectMapper

class SearchModel: BaseModel {

    var text: [String: Search]
    
    required init?(map: Map) {
        fatalError("init(map:) has not been implemented")
    }
    
    override func mapping(map: Map) {
        text <- map["$text"]
    }
}

class Search: BaseModel {
    
    var search: [String: String]
    
    required init?(map: Map) {
        fatalError("init(map:) has not been implemented")
    }
    
    override func mapping(map: Map) {
        search <- map["$search"]
    }
}
