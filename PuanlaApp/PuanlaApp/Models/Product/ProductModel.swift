//
//  Items.swift
//
//  Created by Ferdi Kızıltoprak on 13/03/2017
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

class ProductModel: BaseModel {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let name = "name"
        static let definitionName = "definitionName"
        static let updated = "_updated"
        static let ownersite = "ownersite"
        static let links = "_links"
        static let descriptionValue = "description"
        static let rating = "rating"
        static let brand = "brand"
        static let id = "_id"
        static let etag = "_etag"
        static let comments = "comments"
        static let images = "images"
        static let created = "_created"
        static let category = "category"
    }
    
    // MARK: Properties
    public var name: String?
    public var definitionName: String?
    public var updated: String?
    public var ownersite: String?
    public var links: Links?
    public var descriptionValue: String?
    public var rating: String?
    public var brand: String?
    public var id: String?
    public var etag: String?
    public var comments: [Comments]?
    public var images: String?
    public var created: String?
    public var category: [String]?
    
    override init() {
        super.init()
    }
    
    // MARK: ObjectMapper Initializers
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public required init?(map: Map){
      super.init(map: map)
    }
    
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public override func mapping(map: Map) {
        name <- map[SerializationKeys.name]
        definitionName <- map[SerializationKeys.definitionName]
        updated <- map[SerializationKeys.updated]
        ownersite <- map[SerializationKeys.ownersite]
        links <- map[SerializationKeys.links]
        descriptionValue <- map[SerializationKeys.descriptionValue]
        rating <- map[SerializationKeys.rating]
        brand <- map[SerializationKeys.brand]
        id <- map[SerializationKeys.id]
        etag <- map[SerializationKeys.etag]
        comments <- map[SerializationKeys.comments]
        images <- map[SerializationKeys.images]
        created <- map[SerializationKeys.created]
        category <- map[SerializationKeys.category]
    }
}
