//
//  Images.swift
//
//  Created by Ferdi Kızıltoprak on 13/03/2017
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

class Images: BaseModel {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let isZoomable = "isZoomable"
    static let height = "height"
    static let zoomImageUrl = "zoomImageUrl"
    static let width = "width"
    static let imageUrl = "imageUrl"
    static let maxZoomSize = "maxZoomSize"
    static let url = "url"
    static let thumbnailUrl = "thumbnailUrl"
  }

  // MARK: Properties
  public var isZoomable: Bool? = false
  public var height: Int?
  public var zoomImageUrl: String?
  public var width: Int?
  public var imageUrl: String?
  public var maxZoomSize: Int?
  public var url: String?
  public var thumbnailUrl: String?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){
    super.init(map: map)
  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public override func mapping(map: Map) {
    isZoomable <- map[SerializationKeys.isZoomable]
    height <- map[SerializationKeys.height]
    zoomImageUrl <- map[SerializationKeys.zoomImageUrl]
    width <- map[SerializationKeys.width]
    imageUrl <- map[SerializationKeys.imageUrl]
    maxZoomSize <- map[SerializationKeys.maxZoomSize]
    url <- map[SerializationKeys.url]
    thumbnailUrl <- map[SerializationKeys.thumbnailUrl]
  }
}
