//
//  ProductResponse.swift
//
//  Created by Ferdi Kızıltoprak on 06/03/2017
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

class ProductListResponse: BaseResponse {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let items = "_items"
    static let meta = "_meta"
    static let links = "_links"
  }

  // MARK: Properties
  public var items: [ProductModel]?
  public var meta: Meta?
  public var links: Links?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){
    super.init(map: map)
  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public override func mapping(map: Map) {
    items <- map[SerializationKeys.items]
    meta <- map[SerializationKeys.meta]
    links <- map[SerializationKeys.links]
  }
}
