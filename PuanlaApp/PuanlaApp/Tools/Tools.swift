//
//  Tools.swift
//  PuanlaApp
//
//  Created by Ferdi Kızıltoprak on 27/12/2016.
//  Copyright © 2016 Ferdi Kızıltoprak. All rights reserved.
//

import UIKit

class Tools: NSObject {
    class func showPopup(title: String!, message: String!, buttonTitle: String!, viewController: UIViewController!, handler: ((UIAlertAction) -> Void)? ){
        let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        let okAction = UIAlertAction(title: buttonTitle, style: UIAlertActionStyle.default, handler: handler)
        alertController.addAction(okAction)
        viewController.present(alertController, animated: true, completion: nil)
    }
}
