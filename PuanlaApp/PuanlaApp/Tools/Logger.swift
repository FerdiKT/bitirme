//
//  Logger.swift
//  PuanlaApp
//
//  Created by Ferdi Kızıltoprak on 28/12/2016.
//  Copyright © 2016 Ferdi Kızıltoprak. All rights reserved.
//

import UIKit

class Logger: NSObject {
    
    static var log = true
    class func debug(message: Any){
        if log {
            print(message)
        }
        
    }
}
